# Information / Информация

SPEC-файл для создания RPM-пакета **nginx**.

## Install / Установка

1. Подключить репозиторий **MARKETPLACE**: `dnf copr enable marketplace/nginx`.
2. Установить пакет: `dnf install nginx`.

## Donation / Пожертвование

- [Donating](https://donating.gitlab.io/)